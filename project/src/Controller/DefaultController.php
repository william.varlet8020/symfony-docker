<?php

namespace App\Controller;

use App\Entity\Monument;
use App\Repository\MonumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'default')]
    public function index(MonumentRepository $monumentRepository): Response
    {
        // Get all monuments
        $monuments = $monumentRepository->findAll();

        return $this->render('default/index.html.twig', [
            'monuments' => array_map(fn(Monument $monument) => [
                'id' => $monument->getId(),
                'name' => $monument->getSite(),
                'latitude' => $monument->getLatitude(),
                'longitude' => $monument->getLongitude(),
            ], $monuments),
        ]);
    }
}
