<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Monument;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    #[Route('/comment/{monument}', name: 'comment')]
    public function addComment(Request $request, EntityManagerInterface $manager, Monument $monument): Response
    {
        $comment = new Comment();
        $comment->setMonument($monument);
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($comment);
            $manager->flush();

            return new JsonResponse('Commentaire ok', Response::HTTP_CREATED);
        }

        return $this->render('comment/_form.html.twig', [
            'form' => $form->createView(),
            'comment' => $comment
        ]);
    }
}
