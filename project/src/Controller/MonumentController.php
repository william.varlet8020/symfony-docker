<?php

namespace App\Controller;

use App\Entity\Monument;
use App\Repository\MonumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/monument', name: 'monument_')]
class MonumentController extends AbstractController
{
    #[Route('/{id}', name: 'info')]
    public function info(Monument $monument): Response
    {
        return $this->render('monument/info.html.twig', [
            'monument' => $monument,
        ]);
    }

    // Get list of all monuments as json
    #[Route('/api/list', name: 'api_list')]
    public function list(MonumentRepository $repository): Response
    {
        $monuments = $repository->findAll();
        $json = [];
        foreach ($monuments as $monument) {
            $json[] = [
                'name' => $monument->getSite()
            ];
        }
        return $this->json($json);
    }

    // Find monument by its name
    #[Route('/api/find/{name}', name: 'api_find')]
    public function find(MonumentRepository $repository, string $name): Response
    {
        $monument = $repository->findOneBy(['site' => $name]);
        if (!$monument) {
            return $this->json(['error' => 'Monument not found'], 404);
        }

        // serialize monument as json
        $json = [
            'id' => $monument->getId(),
            'name' => $monument->getSite(),
            'latitude' => $monument->getLatitude(),
            'longitude' => $monument->getLongitude(),
        ];

        return $this->json($json);
    }
}
