<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comment", indexes={@ORM\Index(name="IDX_9474526C493DA1E5", columns={"monument_id"})})
 * @ORM\Entity
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=50, nullable=false)
     */
    private string $username;

    /**
     * @var string|null
     *
     * @ORM\Column(name="message", type="string", length=255, nullable=true)
     */
    private ?string $message;

    /**
     * @var string
     *
     * @ORM\Column(name="score", type="integer", nullable=false)
     */
    private int $score;

    /**
     * @ORM\ManyToOne(targetEntity=Monument::class, inversedBy="comments")
     * @ORM\JoinColumn(name="monument_id", referencedColumnName="id_number")
     */
    private ?Monument $monument;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMonumentId(): string
    {
        return $this->monumentId;
    }

    /**
     * @param string $monumentId
     */
    public function setMonumentId(string $monumentId): void
    {
        $this->monumentId = $monumentId;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getScore(): string
    {
        return $this->score;
    }

    /**
     * @param string $score
     */
    public function setScore(string $score): void
    {
        $this->score = $score;
    }

    public function getMonument(): ?Monument
    {
        return $this->monument;
    }

    public function setMonument(?Monument $monument): self
    {
        $this->monument = $monument;

        return $this;
    }
}
