<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Monument
 *
 * @ORM\Table(name="monument")
 * @ORM\Entity
 */
class Monument
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_number", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=100, nullable=false)
     */
    private string $category;

    /**
     * @var string
     *
     * @ORM\Column(name="criteria_txt", type="string", length=100, nullable=false)
     */
    private string $criteriaTxt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="danger", type="string", length=100, nullable=true)
     */
    private ?string $danger;

    /**
     * @var int
     *
     * @ORM\Column(name="date_inscribed", type="integer", nullable=false)
     */
    private int $dateInscribed;

    /**
     * @var bool
     *
     * @ORM\Column(name="extension", type="boolean", nullable=false)
     */
    private bool $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="http_url", type="string", length=100, nullable=false)
     */
    private string $httpUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="image_url", type="string", length=100, nullable=false)
     */
    private string $imageUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="iso_code", type="string", length=100, nullable=true)
     */
    private ?string $isoCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="justification", type="string", length=5000, nullable=true)
     */
    private ?string $justification;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=15, scale=10, nullable=false)
     */
    private string $latitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="location", type="string", length=1000, nullable=true)
     */
    private ?string $location;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=15, scale=10, nullable=false)
     */
    private string $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=100, nullable=false)
     */
    private string $region;

    /**
     * @var bool
     *
     * @ORM\Column(name="revision", type="boolean", nullable=false)
     */
    private bool $revision;

    /**
     * @var string|null
     *
     * @ORM\Column(name="secondary_dates", type="string", length=100, nullable=true)
     */
    private ?string $secondaryDates;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=5000, nullable=false)
     */
    private string $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=500, nullable=false)
     */
    private string $site;

    /**
     * @var string
     *
     * @ORM\Column(name="states", type="string", length=500, nullable=false)
     */
    private string $states;

    /**
     * @var bool
     *
     * @ORM\Column(name="transboundary", type="boolean", nullable=false)
     */
    private bool $transboundary;

    /**
     * @var int
     *
     * @ORM\Column(name="unique_number", type="integer", nullable=false)
     */
    private int $uniqueNumber;

    /**
     * @ORM\OneToMany(targetEntity=MonumentImage::class, mappedBy="monument")
     * @ORM\JoinColumn(name="id_number", referencedColumnName="monument_id")
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="monument")
     * @ORM\JoinColumn(name="id_number", referencedColumnName="monument_id")
     */
    private $comments;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getCriteriaTxt(): string
    {
        return $this->criteriaTxt;
    }

    /**
     * @param string $criteriaTxt
     */
    public function setCriteriaTxt(string $criteriaTxt): void
    {
        $this->criteriaTxt = $criteriaTxt;
    }

    /**
     * @return string|null
     */
    public function getDanger(): ?string
    {
        return $this->danger;
    }

    /**
     * @param string|null $danger
     */
    public function setDanger(?string $danger): void
    {
        $this->danger = $danger;
    }

    /**
     * @return int
     */
    public function getDateInscribed(): int
    {
        return $this->dateInscribed;
    }

    /**
     * @param int $dateInscribed
     */
    public function setDateInscribed(int $dateInscribed): void
    {
        $this->dateInscribed = $dateInscribed;
    }

    /**
     * @return bool
     */
    public function isExtension(): bool
    {
        return $this->extension;
    }

    /**
     * @param bool $extension
     */
    public function setExtension(bool $extension): void
    {
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getHttpUrl(): string
    {
        return $this->httpUrl;
    }

    /**
     * @param string $httpUrl
     */
    public function setHttpUrl(string $httpUrl): void
    {
        $this->httpUrl = $httpUrl;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return string|null
     */
    public function getIsoCode(): ?string
    {
        return $this->isoCode;
    }

    /**
     * @param string|null $isoCode
     */
    public function setIsoCode(?string $isoCode): void
    {
        $this->isoCode = $isoCode;
    }

    /**
     * @return string|null
     */
    public function getJustification(): ?string
    {
        return $this->justification;
    }

    /**
     * @param string|null $justification
     */
    public function setJustification(?string $justification): void
    {
        $this->justification = $justification;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     */
    public function setLocation(?string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion(string $region): void
    {
        $this->region = $region;
    }

    /**
     * @return bool
     */
    public function isRevision(): bool
    {
        return $this->revision;
    }

    /**
     * @param bool $revision
     */
    public function setRevision(bool $revision): void
    {
        $this->revision = $revision;
    }

    /**
     * @return string|null
     */
    public function getSecondaryDates(): ?string
    {
        return $this->secondaryDates;
    }

    /**
     * @param string|null $secondaryDates
     */
    public function setSecondaryDates(?string $secondaryDates): void
    {
        $this->secondaryDates = $secondaryDates;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     */
    public function setShortDescription(string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return string
     */
    public function getSite(): string
    {
        return $this->site;
    }

    /**
     * @param string $site
     */
    public function setSite(string $site): void
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getStates(): string
    {
        return $this->states;
    }

    /**
     * @param string $states
     */
    public function setStates(string $states): void
    {
        $this->states = $states;
    }

    /**
     * @return bool
     */
    public function isTransboundary(): bool
    {
        return $this->transboundary;
    }

    /**
     * @param bool $transboundary
     */
    public function setTransboundary(bool $transboundary): void
    {
        $this->transboundary = $transboundary;
    }

    /**
     * @return int
     */
    public function getUniqueNumber(): int
    {
        return $this->uniqueNumber;
    }

    /**
     * @param int $uniqueNumber
     */
    public function setUniqueNumber(int $uniqueNumber): void
    {
        $this->uniqueNumber = $uniqueNumber;
    }

    /**
     * @return Collection|MonumentImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(MonumentImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setMonument($this);
        }

        return $this;
    }

    public function removeImage(MonumentImage $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getMonument() === $this) {
                $image->setMonument(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setMonument($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getMonument() === $this) {
                $comment->setMonument(null);
            }
        }

        return $this;
    }

}
