<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Monument
 *
 * @ORM\Table(name="monument_image")
 * @ORM\Entity
 */
class MonumentImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private ?string $url;

    /**
     * @ORM\ManyToOne(targetEntity=Monument::class, inversedBy="images")
     * @ORM\JoinColumn(name="monument_id", referencedColumnName="id_number")
     */
    private ?Monument $monument;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getMonument(): ?Monument
    {
        return $this->monument;
    }

    public function setMonument(?Monument $monument): self
    {
        $this->monument = $monument;

        return $this;
    }

}
