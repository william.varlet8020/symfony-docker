<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220119111629 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, monument_id VARCHAR(25) NOT NULL, username VARCHAR(50) NOT NULL, message VARCHAR(255) DEFAULT NULL, score INT NOT NULL, INDEX IDX_9474526C493DA1E5 (monument_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE monument (id INT AUTO_INCREMENT NOT NULL, category VARCHAR(100) NOT NULL, criteria_txt VARCHAR(100) NOT NULL, danger VARCHAR(100) DEFAULT NULL, date_inscribed INT NOT NULL, extension TINYINT(1) NOT NULL, http_url VARCHAR(100) NOT NULL, id_number INT NOT NULL, image_url VARCHAR(100) NOT NULL, iso_code VARCHAR(100) DEFAULT NULL, justification VARCHAR(5000) DEFAULT NULL, latitude NUMERIC(15, 10) NOT NULL, location VARCHAR(1000) DEFAULT NULL, longitude NUMERIC(15, 10) NOT NULL, region VARCHAR(100) NOT NULL, revision TINYINT(1) NOT NULL, secondary_dates VARCHAR(100) DEFAULT NULL, short_description VARCHAR(5000) NOT NULL, site VARCHAR(500) NOT NULL, states VARCHAR(500) NOT NULL, transboundary TINYINT(1) NOT NULL, unique_number INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE monument');
    }
}
