import {EventBus} from "../utils/event_bus";
import {Controller} from 'stimulus';

import "@tarekraafat/autocomplete.js/dist/css/autoComplete.01.css";
import AutoComplete from "@tarekraafat/autocomplete.js";

export default class extends Controller {
    static targets = ['autocomplete']
    static values = {
        'apiList': String,
        'apiFind': String
    }

    initialize() {
        this.monument = null;
        this.emit = this.emit.bind(this);
    }

    connect() {
        const autoCompleteJS = new AutoComplete({
            data: {
                src: async () => {
                    try {
                        // Loading placeholder text
                        this.autocompleteTarget.setAttribute("placeholder", "Loading...");
                        // Fetch External Data Source
                        const source = await fetch(this.apiListValue);
                        const data = await source.json();
                        // Post Loading placeholder text
                        this.autocompleteTarget.setAttribute("placeholder", autoCompleteJS.placeHolder);
                        // Returns Fetched data
                        return data.map(item => item.name);
                    } catch (error) {
                        return error;
                    }
                },
                cache: true
            },
            placeHolder: "Chercher un monument",
            resultsList: {
                element: (list, data) => {
                    const info = document.createElement("p");
                    if (data.results.length) {
                        info.innerHTML = `Affichage de <strong>${data.results.length}</strong> résultats sur <strong>${data.matches.length}</strong>`;
                    } else {
                        info.innerHTML = `<strong>${data.matches.length}</strong> résultats correspondant à <strong>"${data.query}"</strong>`;
                    }
                    list.prepend(info);
                },
                noResults: true,
                maxResults: 15,
                tabSelect: true,
            },
            resultItem: {
                element: (item, data) => {
                    // Modify Results Item Style
                    item.style = "display: flex; justify-content: space-between;";
                    // Modify Results Item Content
                    item.innerHTML = `
              <span style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
                ${data.match}
              </span>
              <span style="display: flex; align-items: center; font-size: 13px; font-weight: 100; text-transform: uppercase; color: rgba(0,0,0,.2);">
                ${data.key}
              </span>`;
                },
                highlight: true,
            },
            events: {
                input: {
                    selection: async (event) => {
                        autoCompleteJS.input.value = event.detail.selection.value;

                        // get ajax request to get the selected monument
                        const response = await fetch(this.apiFindValue.replace("_search_", event.detail.selection.value));
                        this.monument = await response.json();
                        console.log(this.monument);
                        this.emit();
                    }
                }
            },
        });
    }

    emit() {
        EventBus.emit(`map:fly`, {id: this.monument.id});
    }
}
