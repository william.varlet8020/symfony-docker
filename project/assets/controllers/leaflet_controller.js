import 'leaflet/dist/leaflet.css'
import 'leaflet.markercluster/dist/MarkerCluster.css'
import 'leaflet.markercluster/dist/MarkerCluster.Default.css'

import Sortable from "sortablejs";
import $ from 'jquery'
import * as bootstrap from 'bootstrap';
import L from 'leaflet'
import 'leaflet.markercluster'
import {Controller} from 'stimulus';
import {EventBus} from "../utils/event_bus";

// Chargement des assets de leaflet
import marker2x from 'leaflet/dist/images/marker-icon-2x.png'
import marker from 'leaflet/dist/images/marker-icon.png'
import markerShadow from 'leaflet/dist/images/marker-shadow.png'

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: marker2x,
    iconUrl: marker,
    shadowUrl: markerShadow
});

export default class extends Controller {
    static targets = ['sidebar', 'sidebarContent', 'map']
    static values = {
        'monuments': Array,
        'monumentUrl': String,
        'commentUrl': String
    }

    initialize() {
        this.read = this.read.bind(this);
    }

    connect() {
        this.map = L.map(this.mapTarget).setView([51.505, -0.09], 13);
        this.markers = L.markerClusterGroup()

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            // attribution: '',
            maxZoom: 18,
            minZoom: 1,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoiZHJpZ3RpbWUiLCJhIjoiY2t2b2luMXBzMWhycDMya2xuaHpsbmlmbSJ9.HxGjhUgc0e67QLzVjcX-jQ',
            noWrap: true,
            bounds: [
                [-90, -180],
                [90, 180]
            ]
        }).addTo(this.map);

        this.map.setMaxBounds(
            [
                [-90, -180],
                [90, 180]
            ]
        );

        L.Control.TravelList = L.Control.extend({
            onAdd: (map) => {
                const el = L.DomUtil.create('div', 'leaflet-bar leaflet-control');

                const container = L.DomUtil.create('div', 'card');
                container.style.width = '200px';

                const closeButtonContainer = L.DomUtil.create('div', 'text-end', container);
                const closeButton = L.DomUtil.create('button', 'btn btn-sm', closeButtonContainer);
                closeButton.innerHTML = '<i class="fas fa-times"></i>';

                const list = L.DomUtil.create('ul', 'list-group', container);

                const steps = localStorage.getItem('step');

                if (steps) {
                    const stepsArray = JSON.parse(steps);
                    if (stepsArray.length > 0) {
                        stepsArray.forEach(step => {
                            const listItem = L.DomUtil.create('li', 'list-group-item d-flex align-items-center', list);
                            // inner step.name with a delete button
                            listItem.innerHTML = `<button class="btn btn-sm flex-grow-1" data-action="leaflet#selectStep" data-monument-id="${step.id}">${step.name}</button>`;
                            const deleteButton = L.DomUtil.create('button', 'btn btn-sm', listItem);
                            deleteButton.innerHTML = '<i class="fas fa-times"></i>';
                            deleteButton.onclick = () => {
                                stepsArray.splice(stepsArray.indexOf(step), 1);
                                localStorage.setItem('step', JSON.stringify(stepsArray));
                                this.refreshControlTravelList();
                            }
                        });
                    } else {
                        const listItem = L.DomUtil.create('li', 'list-group-item', list);
                        listItem.innerHTML = '<span>Aucun itinéraire enregistré</span>';
                    }

                    Sortable.create(list, {
                        animation: 150,
                        onEnd: (e) => {
                            // const stepsArray = JSON.parse(localStorage.getItem('step'));
                            const step = stepsArray[e.oldIndex];
                            stepsArray.splice(e.oldIndex, 1);
                            stepsArray.splice(e.newIndex, 0, step);
                            localStorage.setItem('step', JSON.stringify(stepsArray));
                            this.refreshControlTravelList();
                        }
                    });
                }

                closeButton.onclick = function () {
                    const button = L.DomUtil.create('button', 'btn btn-sm btn-light', el);
                    button.innerHTML = '<i class="fas fa-list"></i>';

                    container.remove();
                    el.appendChild(button);

                    button.onclick = () => {
                        el.removeChild(button);
                        el.appendChild(container);
                    }
                }

                el.appendChild(container);

                L.DomEvent.disableClickPropagation(el);
                return el;
            },

            onRemove: function (map) {
                // Nothing to do here
            }
        });

        L.control.travelList = function (opts) {
            return new L.Control.TravelList(opts);
        }

        this.controlTravelList = L.control.travelList({
            position: 'topright'
        }).addTo(this.map);

        // const culturalElement = this.monumentsValue.filter((el) => el.category === "Cultural")
        for (const element of this.monumentsValue) {
            if (element.latitude && element.longitude) {
                this.markers.addLayer(
                    L.marker([element.latitude, element.longitude], {
                        data: element
                    })
                        .bindTooltip(element.name, {
                            direction: 'auto'
                        })
                        .addEventListener('click', (event) => {
                            this.openSideBar(event.sourceTarget.options.data.id)
                        })
                )
            }
        }

        this.map.addLayer(this.markers);

        EventBus.on(`map:fly`, this.read);
    }

    disconnect() {
        EventBus.off(`map:fly`, this.read);
    }

    read(payload = null) {
        if (payload === null) {
            throw new Error("No payload received");
        }

        this.openSideBar(payload.id);

        const monument = this.monumentsValue.find(el => el.id == payload.id);
        if (monument) {
            this.map.flyTo(L.latLng(monument.latitude, monument.longitude));
        }
    }

    selectStep(event) {
        const id = event.target.dataset.monumentId;

        this.openSideBar(id);

        const monument = this.monumentsValue.find(el => el.id == id);
        if (monument) {
            this.map.flyTo(L.latLng(monument.latitude, monument.longitude));
        }
    }

    async openSideBar(id) {
        this.sidebarContentTarget.innerHTML = '...loading';
        $(this.sidebarTarget).show();

        this.sidebarContentTarget.innerHTML = await $.ajax(this.monumentUrlValue.replace('_id_', id));
    }

    closeSideBar() {
        $(this.sidebarTarget).hide();
    }

    refreshControlTravelList() {
        this.controlTravelList.remove();
        this.controlTravelList = L.control.travelList({
            position: 'topright'
        }).addTo(this.map);
    }

    addTravelStep(event) {
        const element = event.target
        const payload = JSON.parse(element.dataset.leafletPayloadParam);
        // get localStorage with key 'step', if not exist, create it, if exist, add to it the payload
        let step = localStorage.getItem('step');
        if (step === null) {
            step = [];
        } else {
            step = JSON.parse(step);
        }

        // Check if the payload is not already in the step
        if (step.find(el => el.id === payload.id) === undefined) {
            step.push(payload);
            localStorage.setItem('step', JSON.stringify(step));
        }

        this.refreshControlTravelList();
    }

    async loadCommentForm(event) {
        const id = event.target.dataset.monumentId

        const monument = this.monumentsValue.find(el => el.id == id);

        $("#exampleModal").find(".modal-title").text(`Ajouter un commentaire pour ${monument.name}`);

        const res = await fetch(this.commentUrlValue.replace('_monument_', id));
        const commentForm = await res.text();

        $("#exampleModal").find(".modal-body").html(commentForm);

        // Catch the form submit event and send the data to the server with ajax request and then close the modal window and open the sidebar
        $("#exampleModal").find("form").submit((e) => {
            e.preventDefault();

            const form = e.target;
            const formData = new FormData(form);

            $.ajax({
                url: form.action,
                type: form.method,
                data: formData,
                processData: false,
                contentType: false,
                success: () => {
                    var myModalEl = document.querySelector('#exampleModal')
                    var modal = bootstrap.Modal.getOrCreateInstance(myModalEl) // Returns a Bootstrap modal instance
                    modal.hide();

                    this.openSideBar(id);
                }
            });
        });
    }
}
